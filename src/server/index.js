const csv = require("csvtojson");
const fs = require("fs");
const MatchCSVPath = "src/data/matches.csv";
const DeliveriesCSVPath = "src/data/deliveries.csv";
const ipl = require("./ipl.js");
const numberOfMatchesPlayedJSONPath =
  "src/public/output/numberOfMatchesPlayed.json";
const numberOfMatchesWinPerYearPath =
  "src/public/output/numberOfMatchesWinPerYear.json";
const extraRunPerTeamPath = "src/public/output/extraRunPerTeam.json";
const economicalBowlersPath = "src/public/output/economicalBowlers.json";

/**
 * Extract total number of matches played every season.
 */
function numberOfMatchesPlayed() {
  csv()
    .fromFile(MatchCSVPath)
    .then((json) => {
      const data = ipl.getNumberOfMatchesPlayed(json);
      fs.writeFile(
        numberOfMatchesPlayedJSONPath,
        JSON.stringify(data),
        (err) => {
          if (err) console.log(err);
        }
      );
    })
    .catch((err) => console.log(err));
}

numberOfMatchesPlayed();

/**
 * Extract number of matches win by every team.
 */
function numberOfMatchesWonPerTeam() {
  csv()
    .fromFile(MatchCSVPath)
    .then((json) => {
      const data = ipl.getNumberOfMatchesWonPerYear(json);
      fs.writeFile(
        numberOfMatchesWinPerYearPath,
        JSON.stringify(data),
        (err) => {
          console.log(err);
        }
      );
    })
    .catch((err) => {
      console.log(err);
    });
}
numberOfMatchesWonPerTeam();

/**
 * Extract Extra Run Per Team.
 */

function extraRunPerTeam() {
  csv()
    .fromFile(MatchCSVPath)
    .then((matchJson) => {
      csv()
        .fromFile(DeliveriesCSVPath)
        .then((deliveryJson) => {
          const data = ipl.getExtraRunPerTeam(matchJson, deliveryJson);
          fs.writeFile(extraRunPerTeamPath, JSON.stringify(data), (err) => {
            if (err) console.log(err);
          });
        })
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
    })
    .catch((err) => {
      if (err) {
        console.log(err);
      }
    });
}
extraRunPerTeam();
/**
 * Extract 10 economical Bowler in year 2015.
 */
function economicalBowler2015() {
  csv()
    .fromFile(MatchCSVPath)
    .then((matchJson) => {
      csv()
        .fromFile(DeliveriesCSVPath)
        .then((deliveryJson) => {
          const data = ipl.getEconomicalBowler2015(matchJson, deliveryJson);
          fs.writeFile(economicalBowlersPath, JSON.stringify(data), (err) => {
            if (err) console.log(err);
          });
        })
        .catch((err) => {
          if (err) {
            console.log(err);
          }
        });
    })
    .catch((err) => {
      if (err) {
        console.log(err);
      }
    });
}

economicalBowler2015();
