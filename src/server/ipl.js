/**
 * Returns objects containing number of matches played every season.
 * @param {*} MatchDetail - contains match details in json format
 */
function getNumberOfMatchesPlayed(MatchDetail) {
  const numberOfMatchObject = {};
  MatchDetail.reduce((numberOfMatch, match) => {
    if (numberOfMatchObject[match["season"]] === undefined) {
      numberOfMatchObject[match["season"]] = 1;
    } else {
      numberOfMatchObject[match["season"]]++;
    }
  });
  return numberOfMatchObject;
}

/**
 * Returns object containing number of matches win by teams.
 * @param {*} MatchDetail -contains match detail in json format
 */
function getNumberOfMatchesWonPerYear(MatchDetail) {
  const numberOfWinnerObject = {};
  MatchDetail.reduce((numberOfWinner, match) => {
    if (numberOfWinnerObject[match["season"]] === undefined) {
      numberOfWinnerObject[match["season"]] = {};
      numberOfWinnerObject[match["season"]][match["winner"]] = 1;
    } else {
      if (
        numberOfWinnerObject[match["season"]][match["winner"]] === undefined
      ) {
        numberOfWinnerObject[match["season"]][match["winner"]] = 1;
      } else {
        numberOfWinnerObject[match["season"]][match["winner"]]++;
      }
    }
  });
  return numberOfWinnerObject;
}

/**
 * Returns object containing extra runs by team in 2016 season.
 * @param {*} MatchDetail - contains match detail in json format
 * @param {*} DeliveryDetail -contains delivery detail in json format
 */
function getExtraRunPerTeam(MatchDetail, DeliveryDetail) {
  const extraRunObject = {};
  let matchId2016 = MatchDetail.filter((match) => match.season == 2016).map(
    (map) => map.id
  );

  DeliveryDetail.reduce((extraRun, delivery) => {
    if (matchId2016.indexOf(delivery.match_id) > -1) {
      if (extraRunObject[delivery["bowling_team"]] === undefined) {
        extraRunObject[delivery["bowling_team"]] = parseInt(
          delivery.extra_runs
        );
      } else {
        extraRunObject[delivery["bowling_team"]] += parseInt(
          delivery.extra_runs
        );
      }
    }
  });
  return extraRunObject;
}

/**
 * Returns object containing top 10 economical bowler.
 * @param {*} MatchDetail -contains match detail in json format
 * @param {*} DeliveryDetail -contains delivery detail in json format
 */

function getEconomicalBowler2015(MatchDetail, DeliveryDetail) {
  const economicalBowler = {};

  let matchId2015 = MatchDetail.filter((match) => match.season == 2015).map(
    (map) => map.id
  );

  DeliveryDetail.reduce((bowler, delivery) => {
    if (matchId2015.indexOf(delivery.match_id) > -1) {
      if (economicalBowler[delivery["bowler"]] === undefined) {
        economicalBowler[delivery["bowler"]] = {};
        economicalBowler[delivery["bowler"]]["balls"] = 1;
        economicalBowler[delivery["bowler"]]["runs"] = parseInt(
          delivery.total_runs
        );
      } else {
        economicalBowler[delivery["bowler"]]["runs"] += parseInt(
          delivery.total_runs
        );
        economicalBowler[delivery["bowler"]]["balls"]++;
        economicalBowler[delivery["bowler"]]["economy"] = (
          economicalBowler[delivery["bowler"]]["runs"] /
          (economicalBowler[delivery["bowler"]]["balls"] / 6)
        ).toFixed(2);
      }
    }
  });

  //contains only economy of bowlers in array
  const economyOfBowlers = [];
  for (let bowlers in economicalBowler) {
    economyOfBowlers.push(economicalBowler[bowlers].economy);
  }
  economyOfBowlers.sort((a, b) => {
    return a - b;
  });

  const topTenBowlers = {};

  for (let index = 0; index <= 10; index++) {
    for (let key in economicalBowler) {
      if (economicalBowler[key].economy === economyOfBowlers[index]) {
        topTenBowlers[key] = economicalBowler[key];
      }
    }
  }
  return topTenBowlers;
}

module.exports = {
  getNumberOfMatchesPlayed,
  getNumberOfMatchesWonPerYear,
  getExtraRunPerTeam,
  getEconomicalBowler2015,
};
